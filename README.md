# HashiCorp Consul install

This role installs the Consul agent in server or client mode. It is mostly based
on the [deployment guide](https://developer.hashicorp.com/consul/tutorials/production-deploy/deployment-guide).

## How to use

The role is tested with basic config on Arch Linux, Ubuntu and AlmaLinux.
I'm not testing a full client/server setup, so don't expect it to "just work".

## How to test

You'll need [Molecule](https://molecule.readthedocs.io/en/latest/installation.html),
the `podman` plugin for Molecule and `ansible-lint`.

Run `molecule test` in the root path of this repository.

The default Molecule test OS is Arch Linux. To run tests on Ubuntu/AlmaLinux,
set the env var `ROLE_TEST_OS` to `ubuntu` or `almalinux`.

## Should I use it?

This role is not meant to be "production ready", but it is good enough to play
with Consul on a hobby project. Read the additional considerations before making
your decision.

If you need a more customizable role, check out [Ansible Galaxy](https://galaxy.ansible.com/).

## Additional considerations

### Extra configuration

The base configuration can be extended by creating a variable named `consul_extra_configuration`
with values in HCL format, see the example in [`defaults`](./defaults/main.yaml).

You can use that to enable the server GUI, auto-join, Consul Connect, or any other settings not
covered in the [configuration template](./templates/consul.hcl.j2).

### Gossip encryption

According to [docs](https://developer.hashicorp.com/consul/docs/agent/config/cli-flags#_encrypt):

> If it is provided after Consul has been initialized with an encryption key, then
> the provided key is ignored and a warning will be displayed.

This role will not do anything to rotate encryption keys for you. You'll need to
do manual intervention.

If you have already created a datacenter without encryption, you'll need manual
intervention to enable it: [docs](https://developer.hashicorp.com/consul/tutorials/security/gossip-encryption-secure#enable-on-an-existing-consul-datacenter).

### TLS encryption

This role will not create the CA and server certificates for you. You must do it separately
and add the correct variables to each server/client inventory.

If you have already created a datacenter withouth TLS encryption, you'll need manual
intervention to enable it: [docs](https://developer.hashicorp.com/consul/tutorials/security-operations/tls-encryption-secure-existing-datacenter).

The molecule test scenarios for TLS contain exposed certificate keys. Don't use them
for anything besides running those tests.

### ACL

Setting up the ACL system requires manual intervention, there are no tasks in this role
to help with the setup.

Read the [docs](https://developer.hashicorp.com/consul/tutorials/security/access-control-setup-production)
for more information on how to do it.
